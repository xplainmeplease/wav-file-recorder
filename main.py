import sys
import os
import design
import re
import microphoneRecorder
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox, QInputDialog, QLineEdit


class wavRecorderApplication(QtWidgets.QMainWindow, design.Ui_MainWindow):
    numbers = ["Один","Два","Три","Четыре","Пять","Шесть","Семь","Восемь","Девять"]
    recordCounters = 0
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.listCurrDirFiles()
        self.btnStart.clicked.connect(self.startRecording)
    def listCurrDirFiles(self):
        self.fileList.clear()
        directory = os.getcwd()
        for fileName in os.listdir(directory):
            if re.match("\w+.wav",fileName):
              self.fileList.addItem(fileName)
    def startRecording(self, recordCoutners):

        msg = QInputDialog.getText(self,"Dialog","File name")

        if msg:
            newMsg = (msg[0]+'.wav')
            r = microphoneRecorder.recorder()
            r.recordData(newMsg)
            self.listCurrDirFiles()


    def finishRecording(self):
        print("Fi")

def main():
    app = QtWidgets.QApplication(sys.argv)
    window = wavRecorderApplication()
    window.show() # show window
    app.exec_() # run application

if __name__ == '__main__':
        main()

