import pyaudio
import wave
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox

class recorder:
    chunk = 1024 # chunk of 1024 samples
    sample_format = pyaudio.paInt16
    channels = 2
    fs = 8000 #samples per second
    seconds = 2

    def recordData(self, fileName_):
        msg = QMessageBox()
        msg.setText("PRESS OK IF YOU'RE READY TO SPEAK!")
        p = pyaudio.PyAudio()
        if(msg.exec() == QMessageBox.Ok):
            print("DEBUG: start recording;")

            stream = p.open(format =  self.sample_format,channels = self.channels, rate = self.fs, frames_per_buffer = self.chunk,input = True)

            frames = [] #storing frames here

            for i in range(0,int(self.fs/self.chunk*self.seconds)):
                data = stream.read(self.chunk)
                frames.append(data)

            stream.stop_stream()
            stream.close()
            p.terminate()
            print("DEBUG: stop recording;")


            print("DEBUG: start writting file;")
            wf = wave.open(fileName_, 'wb')
            wf.setnchannels(self.channels)
            wf.setsampwidth(p.get_sample_size(self.sample_format))
            wf.setframerate(self.fs)
            wf.writeframes(b''.join(frames))
            wf.close()
            print("DEBUG: stop writting file;")
        print("DEBUG: stop writting file;")




