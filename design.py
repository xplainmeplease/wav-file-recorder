# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'design/design.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(389, 266)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.fileList = QtWidgets.QListWidget(self.centralwidget)
        self.fileList.setGeometry(QtCore.QRect(10, 10, 231, 251))
        self.fileList.setObjectName("fileList")
        self.btnStart = QtWidgets.QPushButton(self.centralwidget)
        self.btnStart.setGeometry(QtCore.QRect(250, 10, 131, 27))
        self.btnStart.setFlat(False)
        self.btnStart.setObjectName("btnStart")
        self.durationLabel = QtWidgets.QLabel(self.centralwidget)
        self.durationLabel.setGeometry(QtCore.QRect(250, 50, 111, 19))
        self.durationLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.durationLabel.setObjectName("durationLabel")
        self.durationLabelTime = QtWidgets.QLabel(self.centralwidget)
        self.durationLabelTime.setGeometry(QtCore.QRect(370, 50, 67, 19))
        self.durationLabelTime.setObjectName("durationLabelTime")
        self.manualButton = QtWidgets.QPushButton(self.centralwidget)
        self.manualButton.setGeometry(QtCore.QRect(250, 230, 131, 27))
        self.manualButton.setObjectName("manualButton")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btnStart.setText(_translate("MainWindow", "Record"))
        self.durationLabel.setText(_translate("MainWindow", "Record duration"))
        self.durationLabelTime.setText(_translate("MainWindow", "3s"))
        self.manualButton.setText(_translate("MainWindow", "!!Manual!1!"))
